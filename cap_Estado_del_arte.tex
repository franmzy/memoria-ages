% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------

\chapter{Estado del arte}
\label{sec:estadoArte}

% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------



\section{Realidad aumentada}

\subsection{Definición de realidad aumentada}

La tecnología de Realidad Virtual (RV) produce la completa inmersión del usuario dentro de un mundo sintético o virtual. Mientras que esta inmerso en este mundo ficticio, el usuario es incapaz de ver el mundo real que le rodea. Por el contrario, la RA permite al usuario observar el mundo real, con objetos virtuales superpuestos o complementando el mundo real. La RA complementa la realidad, en vez de reemplazarla completamente. De forma ideal, al usuario le parecerá que objetos virtuales y reales coexisten en el mismo espacio (\cite{azuma_survey_1997-1}). Tanto la realidad aumentada como la virtualización aumentada forman parte de lo que es llamado realidad mixta (Fig. \ref{fig:realidad_cont}).

\begin{figure}[h]
    \centering
    \includegraphics[width=.5\textwidth]{realidad_cont}
    \caption{Esquema de los estados intermedios entre el entorno virtual y el real.}
    \label{fig:realidad_cont}
\end{figure}

Se puede definir la RA como un sistema que cumple las siguientes características:

\begin{itemize}
	\item Combina objetos reales y virtuales en un entorno real.
	\item Es un sistema interactivo en tiempo real.
	\item El registro en tres dimensiones.
\end{itemize}

Existen tres aspectos importantes a mencionar. Esta definición no se restringe a una tecnología de pantalla específica, como pueden ser los cascos de realidad virtual o HMD (del inglés head-mounted display). No se aplica únicamente al sentido de la vista, ya que el término de RA puedes ser aplicado al resto de los sentidos, incluidos el oído, el tacto y el olfato. Y por último, la eliminación de objetos reales superponiendo objetos virtuales, aproximación llamada realidad disminuida (\textit{diminished reality}), también es considerada RA (\cite{van_krevelen_survey_2010}).



\subsection{Un poco de historia}

Para empezar debemos entender que la evolución de la Realidad Virtual y Aumentada han ido prácticamente de la mano durante los últimos cincuenta años. Los primeros prototipos de RA (Fig. \ref{fig:primer_prot}) fueron creados por los pioneros en computación gráfica Ivan Sutherland y sus estudiantes en la universidad de Harvard y de la universidad de Utah. Estos prototipos aparecieron en la década de los sesenta y utilizaban uno casco con unas lentes, conectado a sensores mecánicos de movimiento, donde se representaban gráficos 3D (\cite{sutherland_head-mounted_1968}). Se trataba de un sistema muy primitivo en cuanto a interfaz de usuario y realismo.


\begin{figure}[hbt]
    \centering
    \includegraphics[width=.5\textwidth]{casco_ivan}
    \caption{El primer casco de realidad aumentada de la historia}
    \label{fig:primer_prot}
\end{figure}


A mediados de 1970, \citeauthor{krueger_videoplaceartificial_1985} crearon un laboratorio de realidad aumentada llamado \textit{Videoplace}. Su idea con este proyecto era la creación de una realidad artificial que envolviera al usuario, y respondiera a sus movimientos sin necesidad de utilizar gafas o guantes para ello. \textit{Videoplace} usaba proyectores, cámaras de video y siluetas de los usuarios proyectadas en pantallas para situar al usuario en un entorno interactivo.


\begin{figure}[hbt]
    \centering
    \includegraphics[width=.5\textwidth]{videoplace}
    \caption{Funcionamiento de \textit{Videoplace}}
    \label{fig:videoplace}
\end{figure}


El termino 'realidad virtual' es atribuido a Jaron Lanier en el año 1989 y no es hasta un año después cuando \citeauthor{caudell_augmented_1992-1}, científicos de Boeing Corporation, acuña el término 'realidad aumentada'; quienes desarrollaron un sistema de RA experimental para ayudar a los trabajadores a manipular cableado. La verdadera RA estaba fuera de alcance debido a la falta de tecnología y perdió el interés de los usuarios debido al hueco formado entre las expectativas del público y las limitaciones de la tecnología. Pero pronto los ordenadores y los dispositivos de reconocimiento llegaron a ser lo suficientemente potentes y pequeños para soportar el uso de la RA en ellos. \citeauthor{feiner_touring_1997} crearon uno de los primeros prototipos de  un sistema de RA móvil (MARS) que conseguía renderizar información de una visita guiada, con la información de los edificios y demás elementos que podían ser observados por el visitante.

A finales de los noventa, como la RA se había convertido en un campo merecedor de investigación, se crearon diferentes conferencias de RA, incluyendo ISMAR (International Symposium on Mixed and Augmented Reality). Además se convirtió relativamente fácil desarrollar aplicaciones de RA gracias a la existencia de herramientas de código libre como eran \textit{ARToolKit}. Durante este tiempo surgieron diferentes ensayos en los que se explicaban los progresos en esta materia, describían sus problemas, clasificaban y resumían su desarrollo (\cite{azuma_survey_1997-1}, \cite{azuma_recent_2001-2}, \cite{benford_understanding_1998}).




\subsection{Situación actual de la realidad aumentada}
\label{sec:sit_rea_aumentada}

En la última década, la barrera tecnológica que no dejaba avanzar a la realidad aumentada, ha ido desapareciendo o situándose más lejos. El mercado y ámbito de investigación de la realidad aumentada se encuentra en pleno crecimiento. Algunos estudios auguran una tasa de crecimiento anual de un 78\% (\cite{_abi_????}). 

Uno de los factores que han propiciado este crecimiento, es la aparición de nuevos dispositivos capaces de soportar la ejecución de aplicaciones de realidad aumentada. Uno de estos dispositivos, son los teléfonos inteligentes. Su capacidad de procesamiento, la cual aumenta cada año, y la disponibilidad de una cámara de video, hace posible su uso en el ámbito de la realidad aumentada. El uso de los teléfonos inteligentes también se encuentra en actual crecimiento y esto ha hecho capaz a la realidad aumentada llegar a una gran cantidad de público nuevo.

Los investigadores y desarrolladores también han sido cómplices de este crecimiento. Se han desarrollado diferentes kits de herramienta para implementar aplicaciones de realidad aumentada. Entre los más conocidos se encuentran \textit{ARToolKit}\footnote{https://ARToolKit.org/}, \textit{Wikitude}\footnote{http://www.wikitude.com/} o \textit{Vuforia}\footnote{http://www.vuforia.com/}, o la española \textit{ARlab}\footnote{http://www.arlab.com/}. El continuo desarrollo de herramientas está haciendo que el uso de la realidad aumentada cada vez sea más preciso y con característica más revolucionarias.


La mayoría de estos kits de herramientas, usan unas imágenes, llamadas \textit{targets}, como punto de referencia en el mundo real, para dibujar imágenes u objetos 3D sobre ellas como si realmente se encontraran en su superficie. \textit{PlayStation} en 2009 llevó este tipo de realidad aumentada a los videojuegos con su juego \textit{invizimals}, en el cual a través de la cámara de su consola, se renderizan personajes en 3D que luchan entre ellos. Este juego fue galardonado por su innovación en el sector del videojuego. Otras compañía de videojuegos, \textit{XBOX - Microsoft} optó por utilizar la virtualización aumentada junto con reconocimiento corporal. En esta tecnología no se realiza uso de \textit{targets} sino que se toma el cuerpo del usuario como referencia para interactuar con el mundo virtual. Esto fue posible gracias a su sensor \textit{Kinect} que incorpora una cámara de color, una cámara de infrarrojos y un proyector de infrarrojos. Esta cámara posteriormente sí que ha servido para realizar estudios y proyectos con realidad aumentada (\cite{casas_kinect-based_2012}).


\begin{figure}[hbt]%
    \centering
    \subfloat[Targets en \textit{Vuforia}.]{{\includegraphics[width=6.75cm]{vuforia-target} }}%
    \qquad
    \subfloat[Juego de realidad aumentada, \textit{Invizimals}.]{{\includegraphics[width=6.75cm]{inivizimals} }}%
    \caption{Realidad aumentada con \textit{targets}.}%
    \label{fig:target-tech}%
\end{figure}


Aunque las compañías de software actualmente están apostando más por el uso de la realidad virtual, muchas de ellas también están invirtiendo en la investigación de nuevos usos de la realidad aumentada. Un ejemplo de estas compañías es \textit{Google} que ya en 2013 lanzo su primer prototipo de las llamadas \textit{Google Glass}. Se trata de un casco de realidad aumentada diseñado en forma de gafas. Es un dispositivo \textit{Android} controlado por voz que muestra información directamente en el campo de visión del usuario. 

\begin{figure}[hbt]
    \centering
    \includegraphics[width=1\textwidth]{google-glass}
    \caption{\textit{Google Glass}}
    \label{fig:glass}
\end{figure}

Sin embargo este dispositivo solo recibe información del entorno a través de su cámara, sus sensores de movimiento, sensor geomagnético o GPS. En la mayoría de los casos esa información no es suficiente para reconocer obstáculos u objetos en la escena y por lo tanto no es posible interactuar con ellos. \textit{Google Glass} es un dispositivo excelente para el uso de la computación ubicua, sin embargo tiene ciertas carencias que \textit{Google} ha querido suplir con la presentación en junio de 2015 de \textit{Project Tango}\footnote{https://get.google.com/tango/}. Se trata de un sistema en el que adicionalmente se ha incorporado un sensor de profundidad. Lo que se quiere conseguir con este proyecto es ser capaz de capturar el movimiento del dispositivo, usar la visión para aprender el entorno y reconocerlo una vez aprendido, y reconocer las formas y obstáculos del entorno gracias a su sensor de profundidad.


\begin{figure}[hbt]
    \centering
    \includegraphics[width=.65\textwidth]{tango}
    \caption{Captura del \textit{Project Tango}.}
    \label{fig:tango}
\end{figure}
		

Este no es el único proyecto vanguardista que hace uso de este tipo de sensores para ubicar un dispositivo en el entorno. Recientemente \textit{Microsoft} ha anunciado un casco de realidad aumentada que puede llegar a ser el más inmersivo hasta la fecha, llamado \textit{HoloLens}\footnote{https://www.microsoft.com/microsoft-hololens/} (Figura \ref{fig:hololens}). Este dispositivo está formado por unas lentes holográficas transparentes, sobre las cuales se proyecta el entorno virtual, sensores de movimiento, un sensor de profundidad y cuatro 'sensores de comprensión del entorno' (\citeauthor{holmdahl_build_2015}, 2015). El objetivo principal al construir este dispositivo es el de ser capaces de reconocer de qué está formado el entorno, su composición y estructura, para así ser capaces de tener una total interacción con él. Algunas simulaciones muestran juegos que interactúan con los objetos de una habitación, objetos virtuales que conservan su posición en el mundo real sin necesidad de \textit{targets} o superposición de objetos como pantallas sobre las paredes de una habitación.


\begin{figure}[hbt]%
    \centering
    \subfloat[Hardware \textit{Hololens}]{{\includegraphics[width=6.75cm]{hololens-hardware} }}%
    \qquad
    \subfloat[Simulación \textit{Hololens}]{{\includegraphics[width=6.75cm]{Hololens} }}%
    \caption{Casco de realidad aumentada \textit{Hololens}.}%
    \label{fig:hololens}%
\end{figure}


Se puede apreciar, que el ámbito de estudio de la realidad aumentada, ha ido adoptando una dirección: el conocimiento del entorno. Nuevos proyectos como \textit{Tango} u \textit{Hololens} tienen una misión clara, reconocer el entorno donde se encuentran y la posición que ocupan en él, para así interactuar con este, mediante la realidad aumentada, de la forma más precisa posible. Es decir, geolocalizar el dispositivo móvil, en un entorno cerrado, del cuál sabemos su estructura. 

\section{Sistemas de posicionamiento en interiores}

\subsection{Definición de posicionamiento en interiores}


Un sistema de posicionamiento en entornos cerrados (o por sus siglas en inglés IPS) considera solo entornos cerrados, tal como el interior de un edificio. \citeauthor{dempsey_location_2012} (\citeyear{dempsey_location_2012}) en su artículo '\citetitle{dempsey_location_2012}' define un IPS como un sistema que continuamente y en tiempo real puede determinar la posición de algo o alguien en un espacio físico como un hospital, gimnasio, escuela, etc. De esta definición se puede extraer que un IPS debe funcionar durante todo el tiempo a no ser que el usuario lo desconecte, debe ofrecer información actualizada de la posición del objetivo, estimando esta posición con un máximo tiempo de retraso y poder cubrir todo el área que el usuario requiera para utilizar el sistema.

Un IPS puede proporcionar diferente tipo de información sobre localización a aplicaciones que la requieran, pero antes de que esta información pueda ser estimada, el mapa del área que se quiera localizar debe estar previamente disponible y guardado en el IPS. Con respecto a este mapa, existen tres tipos de información que podemos recibir de un sistema IPS (\cite{gu_survey_2009}):

\begin{itemize}
	\item \textbf{Posición absoluta:} A menudo los sistemas IPS ofrecen la información de la posición absoluta del objetivo con respecto al área que cubre el mapa, ya que los sistemas de rastreado y guiado necesitan la posición exacta del objetivo.
	\item \textbf{Posición relativa:} La posición relativa es otro tipo de información ofrecida por los IPS, que controla el movimiento de diferentes partes de un objetivo. Por ejemplo, un IPS que controla si la puerta de un coche se encuentra cerrada, necesita tener la información sobre cuál es la posición relativa del punto objetivo situado en la puerta, respecto al cuerpo del coche.
	\item \textbf{Información de proximidad:} En ocasiones no es necesario que un sistema IPS informe de la posición absoluta o relativa de un objetivo. La información de proximidad puede ser suficiente por ejemplo para saber si un paciente entra en la habitación correcta para un diagnóstico u operación.
\end{itemize}
 
 
 
 
 \subsection{Tecnologías y técnicas IPS}
 
Los IPS tienen la necesidad de hacer capaces a los sistemas de computación de entender el entorno donde se ejecutan. Ciertas tecnologías inalámbricas han sido desarrolladas para la percepción de estos entornos. Estas tecnologías incluyen sistemas de infrarojos (IR), ultrasonidos, identificación por radiofrecuencia (RFID), red de área local inalámbrica (WLAN), Bluetooth, banda ultra ancha(UWB), tecnología magnética, etc. Cada tecnología tiene ventajas únicas en el comportamiento para obtener localización en espacios cerrados. Por ejemplo, un IPS usando tecnología WLAN no necesita la incorporación de nuevas infraestructuras, ya que puede utilizar su propia red ampliamente implantada. En cambio el uso de esta tecnología sufre de ciertos inconvenientes debido a sus propiedades.

Equipados con una o varias tecnologías de localización, los IPS usan diferentes técnicas para localizar objetos y ofrecer su posición, absoluta, relativa y proximidad al objetivo. Hay cuatro técnicas para la estimación de la posición en entornos cerrados: triangulación, huella digital(\textit{Fingerprints}), proximidad y análisis visual (\cite{hightower_location_2001}).


La triangulación, está basada en la geometría de los triángulos. Tres métodos pueden ser utilizados para calcular la posición: indicador de fuerza de la señal recibida (RSS), ángulo de llegada de la señal (AOA) y tiempo de llegada (TOA) (\cite{vossiek_wireless_2003}).


Se utilizan las llamadas técnicas de huellas digitales para mejorar la precisión de mediciones de la posición en interiores. Esto es posible gracias al uso de información de datos previamente medidas. La técnica de huellas digitales incluye dos fases: la fase de entrenamiento \textit{offline} y la fase de posicionamiento (\cite{kaemarungsi_properties_2004-1}). En la fase \textit{offline}, se recoge información relevante respectiva a diferentes lugares en el área estimada de posicionamiento, en este área se miden las diferentes señales que se puedan captar. En la siguiente fase de reconocimiento, se usa toda la información recogida para poder hacer una estimación de la posición del dispositivo.


\begin{figure}[hbt]%
    \centering
    \subfloat[Triangularización]{{\includegraphics[width=6.75cm]{triangle} }}%
    \qquad
    \subfloat[Esperimento \textit{WLAN}]{{\includegraphics[width=6.75cm]{wlan} }}%
    \caption{Técnicas de localización}%
    \label{fig:tec-geo-1}%
\end{figure}


La técnica de localización por proximidad examina la localización de un objetivo con respecto a una posición de un área ya conocida. En esta técnica se necesita colocar cierto número de sensores en posiciones conocidas. Cuando un objetivo es reconocido por uno de estos sensores, la posición del objetivos se considera que esta en las proximidades del área alcanzada por el detector. En la figura \ref{fig:tec-geo-2}a podemos ver como el objeto $E_2$ está localizado en el área definida por el sensor D, mientras que $E_3$ no lo está.

El análisis visual estima la localización a partir de la imagen recibida de uno o múltiples puntos (\cite{hightower_location_2001}). El análisis visual aporta comodidad y eficiencia a los usuarios, ya que no hay necesidad de que el usuario disponga de un dispositivo extra para el reconocimiento. Normalmente, una o varias cámaras están fijas en el área de posicionamiento de un IPS para cubrir el espacio entero y tomar imágenes en tiempo real. El objetivo es identificado a partir de la imágenes.

\begin{figure}[hbt]%
    \centering
    \subfloat[Experimento de sensores]{{\includegraphics[width=6.75cm]{sensor} }}%
    \qquad
    \subfloat[Análisis visual]{{\includegraphics[width=6.75cm]{vision-analysis} }}%
    \caption{Técnicas de localización}%
    \label{fig:tec-geo-2}%
\end{figure}
  
 Cabe añadir que existen algoritmos de localización especialmente diseñados para calcular la posición de objetivos. Algunos investigadores han desarrollado diferentes algoritmos que mejoran la precisión en el cálculo de la localización.


\subsection{Situación actual del posicionamiento en interiores}

Hace no mucho tiempo, que se ha incrementado el interés por las técnicas de posicionamiento en interiores. El sistema de posicionamiento global (GPS) y el servicio de apoyo en emergencias Enhanced 911 (E-911) solucionan gran parte de la problemática que acarrea la geolocalización. Sin embargo, estas tecnologías no pueden proveernos de geolocalización precisa o fina en interiores. Normalmente dentro de un edificio la localización no es lo suficientemente precisa para ser útil. La señal de los satélites se atenúa y dispersa por el tejado, paredes u otros objetos. Además, el rango de error de la mayorías de chips GPS puede ser mayor incluso que el espacio cerrado.

A raíz de los logros de la localización basada en satélites en aplicaciones al aire libre, el desafío se ha trasladado a la prestación de tales servicios en interiores. Sin embargo, la capacidad para localizar objetos y personas en el interior sigue siendo un reto importante, que forma un gran cuello de botella para conseguir la correcta geolocalización en todos los ambientes sin problemas. Muchas aplicaciones, necesitadas de un correcto posicionamiento en interiores, se encuentran a la espera de una solución técnica satisfactoria. La localización en interiores es una tecnología novel, que está generando una gran demanda. Esta tecnología tiene su propio mercado y retos tecnológicos únicos.

La investigación en este campo sigue muy activa y su integración con la realidad aumentada es un hecho que se puede observar en proyectos como \textit{Project Tango} u \textit{Hololens}.



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------


\section{Propuesta}
\label{sec:estadoArte:propuesta}

La intención de este TFG es proponer una técnica adicional en el ámbito del posicionamiento en interiores. Se desea utilizar tecnología de realidad aumentada para conseguir este objetivo, en particular las técnicas de reconocimiento de \textit{targets} explicadas en el punto \ref{sec:sit_rea_aumentada}.

Como se ha visto, la mayoría de las técnicas de realidad aumentada requieren un punto de referencia en el mundo real, esto es necesario para que los objetos virtuales que se añadan al entorno tengan concordancia con él. Se han descrito anteriormente como alguna aplicaciones que usan  \textit{targets} como punto de referencia y realizan sus proyecciones sobre los planos donde se sitúan. Sin embargo, las utilidades de este tipo de referencia son limitadas y no pueden ir mucho más allá de proyectar objetos en su superficie. También se ha visto cómo utilizar el entorno como referencia puede dar lugar a un conjunto de nuevas aplicaciones.

Este trabajo propone usar la tecnología de reconocimiento de \textit{targets} para parametrizar el entorno y usarlo como referencia; de esta forma poder localizar un dispositivo móvil en el interior de un espacio cerrado. Se trataría de un tipo de posicionamiento preciso, accesible por cualquier usuario que disponga de un dispositivo móvil con cámara y una impresora para disponer de \textit{targets}.

Este tipo de tecnología podría ser aplicada en diferentes ámbitos. Algunos de estos ámbitos podrían ser:
\begin{itemize}
	\item La enseñanza, mejorando la comprensión de los alumnos a través de clases interactivas, en las que se pueda visualizar gráficamente objetos de estudio en el aula.
	\item Tratamiento de traumas, utilizando entornos inmersivos para los pacientes.
	\item Simulaciones de situaciones con riesgo, para el entrenamiento de ciertos profesionales.
	\item En el sector de los videojuegos, ofreciendo la capacidad de interactuar con el entorno donde se juegue, o bien transformando una habitación en el propio escenario de juego.
\end{itemize}

% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% Fin del capítulo
